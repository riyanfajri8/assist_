
var myApp =  angular.module("myItems", ['lbServices']);
myApp.controller("myItemsController", function($scope,$http,Item){
    $scope.items = Item.find();
    $scope.newItem = '';
    $scope.pushItem = function(){
        if($scope.newItem != ""){
            Item.create({name: $scope.newItem
                                                
                        }).$promise.then(function(name){
                $scope.items.push(name);
                $scope.newItem = '';
            })
        }
    }
    $scope.deleteItem = function(index){
        Item.deleteById({id: $scope.items[index].id}).$promise.then(function() {
            $scope.items.splice(index, 1);
            $scope.items = Item.find();
            $scope.newItem = '';
        })
    }
});

var myApp =  angular.module("myPolys", ['lbServices']);
myApp.controller("myPolyController", function($scope,$http,Poly){
    $scope.polys = Poly.find();
    $scope.newPoly = '';
    $scope.pushPoly = function(){
        if($scope.newPoly != ""){
            Poly.create({nama_poly: $scope.newPoly
                                                
                        }).$promise.then(function(nama_poly){
                $scope.polys.push(nama_poly);
                $scope.newPoly = '';
            })
        }
    }
    $scope.deletePoly = function(index){
        Poly.deleteById({id: $scope.polys[index].id}).$promise.then(function() {
            $scope.polys.splice(index, 1);
            $scope.polys = Poly.find();
            $scope.newPoly = '';
        })
    }
});

var myApp =  angular.module("myJadwal_dokters", ['lbServices']);
myApp.controller("myJadwal_DokterController", function($scope,$http,Jadwal_dokter){
    $scope.jadwal_dokters = Jadwal_dokter.find();
    $scope.pushJadwal_dokter = function(){
        if($scope.newdokter != ""){
            Jadwal_dokter.create({dokter: $scope.newdokter,
                         hari :$scope.newHari,
                         jam: $scope.newJam      
                        }).$promise.then(function(dokter,hari,jam){
                $scope.jadwal_dokters = Jadwal_dokter.find();
            })
        }
    }
    $scope.deleteJadwal_dokter = function(index){
        Jadwal_dokter.deleteById({id: $scope.jadwal_dokters[index].id}).$promise.then(function() {
            $scope.jadwal_dokters.splice(index, 1);
            $scope.jadwal_dokters = Jadwal_dokter.find();
            $scope.myJadwal_dokters = '';
        })
    }
});

    var myApp =  angular.module("myRegiss", ['lbServices']);
    myApp.controller("myRegisController", function($scope,$http,Regis,Jadwal_dokter,Poly){
        $scope.regiss = Regis.find();
        $scope.dokters = Jadwal_dokter.find();
        $scope.polys = Poly.find();
        $scope.pushRegis = function(){
            var id_dokter = '';
            var regis = '';
            var hari = '';
            var jam = '';
            var regiss= '';
            var namadokter = '';
            for (var index = 0; index < $scope.dokters.length ; index++) {
                if($scope.dokters[index].id == $scope.newdokter){
                    id_dokter =  $scope.dokters[index].id;
                    namadokter = $scope.dokters[index].dokter;
                    hari =  $scope.dokters[index].hari;
                    jam =  $scope.dokters[index].jam;
                }
                
            }
            for (var index1 = 0; index1 < $scope.regiss.length ; index1++) {
                if($scope.regiss[index1].hari == hari && $scope.regiss[index1].jam == jam){
                    alert('Jadwal Penuh');
                    regiss = 'tidak boleh';
                    return false;
                }else{
                    regiss = 'boleh';   
                }
            }
            if(regiss=='boleh'){
                
                Regis.create({  nama  : $scope.newnama,
                    dokter: namadokter,
                    hari  : hari,
                    jam   : jam,
                    poly  : $scope.newpoly
                }).$promise.then(function(nama,dokter,hari,jam){
                    $scope.regiss = Regis.find();
                    $scope.regiss.push(nama);
                    $scope.regiss.push(dokter);
                    $scope.regiss.push(hari);
                    $scope.regiss.push(jam);
                })
                socket.emit('send data', 'call');
            }
            if($scope.regiss.length == 0 ) {
                var no = 0;
                while(no == 0) {
                    Regis.create({  nama  : $scope.newnama,
                        dokter: namadokter,
                        hari  : hari,
                        jam   : jam,
                        poly  : $scope.newpoly
                    }).$promise.then(function(nama,dokter,hari,jam){
                        $scope.regiss = Regis.find();
                    })
                    no++
                }
                socket.emit('send data', 'call');
                no = 0;
                return false;
            }
            
        }
        $scope.deleteRegis = function(index){
            Regis.deleteById({id: $scope.regiss[index].id}).$promise.then(function() {
                $scope.regiss.splice(index, 1);
                $scope.regiss = $scope.regiss.find();
                $scope.regiss = '';
            })
            socket.emit('send data', 'call');
        }
    });
