'use strict';

module.exports = function(Jadwaldokter) {
  Jadwaldokter.getListJadwalDokter = async function (filter, skip, limit, sort, options) {

    try {
      const {connector, ObjectID}= Jadwaldokter.getDataSource();
      const itemCollection = connector.collection('jadwal_dokter');
      const $$dokterId = ObjectID(filter['dokterId']);
      const itemCursor = await itemCollection.aggregate([
        {
          $match:{
            //isActive:true
          }
        },
        {
          $lookup:{
            from: "dokter",
            localField: "dokterId",
            foreignField: "_id",
            as: "dokters"
          }
        },
        {
          $unwind:{path: '$dokters', preserveNullAndEmptyArrays: true},
        },
        {
          $project:{
            _id : 1,
            hari:1,
            jam:1,
            nama_dokter:"$dokters.nama_dokter"
          }
        },
        {
          $match:{
            $or : [
              {nama_dokter: {$regex: filter['search'], $options: "i"}},
            ]
          }
        }
      ])
      var result = await itemCursor.toArray();

      return Promise.resolve({status: "success", result, size: result.length})
    } catch (err) {
      return Promise.reject(err);
    }
  }
  Jadwaldokter.remoteMethod(
    "getListJadwalDokter", {
    description: ["Return Regis filtered by keyword if exists"],
    accepts: [
      { arg: "filter", type: "object", required: true, description: "Filter sample \{\"title\":\"\"\}" },
      { arg: "skip", type: "number", required: false, description: "skip" },
      { arg: "limit", type: "number", required: false, description: "limit" },
      { arg: "sort", type: "string", required: false, description: "sort" },
      {arg: "options", type: "object", http: "optionsFromRequest"},
    ],
    returns: {
      arg: "status", type: "object", root: true, description: "Return value"
    },
    http: {verb: "get"}
  });

  Jadwaldokter.add = async function (data, options) {
    // payload: {id: "string"}, {data}
    console.log(data);
    try {
      var jadwal_dokter = await Jadwaldokter.create(data);
        return Promise.resolve({status: "success", item: jadwal_dokter});
      } catch (err) {
        return Promise.reject(err);
      }
    }
  Jadwaldokter.remoteMethod(
      "add", {
        description: ["Return updated id"],
        accepts: [
          {arg: "data", type: "object", http: {source: 'body'}, required: true, description: "name, namePic, hpPic, address (object => street, region, city, district, postcode)"},
          {arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: {
          arg: "status", type: "object", root: true, description: "Return value"
        },
        http: {verb: "post", path: "/add"}
      }
  );
};
