'use strict';

const Moment = require("moment");
module.exports = function(Regis) {

  Regis.getListV2 = async function (filter, skip, limit, sort, options) {

    try {
      const {connector, ObjectID}= Regis.getDataSource();
      const itemCollection = connector.collection('Regis');
      const $$polyId = ObjectID(filter['polyId']);
      const $$dokter = ObjectID(filter['dokterId']);
      const itemCursor = await itemCollection.aggregate([
        {
          $match:{
            //isActive:true
          }
        },
        {
          $lookup:{
            from: "poly",
            localField: "polyId",
            foreignField: "_id",
            as: "polys"
          }
        },
        {
          $unwind:{path: '$polys', preserveNullAndEmptyArrays: true},
        },
        {
          $lookup:{
            from: "jadwal_dokter",
            localField: "jadwal_dokterId",
            foreignField: "_id",
            as: "jadwal_dokters"
          },
        },
        {
          $unwind:{path: '$jadwal_dokters', preserveNullAndEmptyArrays: true},
        },
           {
          $lookup:{
            from: "dokter",
            localField: "jadwal_dokters.dokterId",
            foreignField: "_id",
            as: "jadwal_dokters.dokters"
          },
        },
        // {
        //   $unwind:{path: '$dokters', preserveNullAndEmptyArrays: true},
        // },
        // {
        //   $project:{
        //     _id : 1,
        //     nama : 1,
        //     id_dokter:"$dokters.dokterId",
        //     nama_dokter:"$dokters.nama_dokter",
        //     hari:"$dokters.hari",
        //     jam:"$dokters.jam",
        //     nama_poly: "$polys.nama_poly",
        //     isActive: "$polys.isActive"
        //   }
        // },
        // {
        //   $match:{
        //     $or : [
        //       {nama_poly: {$regex: filter['search'], $options: "i"}},
        //       {nama: {$regex: filter['search'], $options: "i"}},

        //     ]
        //   }
        // }
      ])
      var result = await itemCursor.toArray();
      //console.log(result)
      return Promise.resolve({status: "success", result, size: result.length})
    } catch (err) {
      return Promise.reject(err);
    }
  }
  Regis.remoteMethod(
    "getListV2", {
    description: ["Return Regis filtered by keyword if exists"],
    accepts: [
      { arg: "filter", type: "object", required: true, description: "Filter sample \{\"title\":\"\"\}" },
      { arg: "skip", type: "number", required: false, description: "skip" },
      { arg: "limit", type: "number", required: false, description: "limit" },
      { arg: "sort", type: "string", required: false, description: "sort" },
      {arg: "options", type: "object", http: "optionsFromRequest"},
    ],
    returns: {
      arg: "status", type: "object", root: true, description: "Return value"
    },
    http: {verb: "get"}
  });

  Regis.edit = async function (id, data, options) {
    // payload: {id: "string"}, {data}
    try {
      var regis = await Regis.updateAll({id: id}, data);
      if (regis['count'] > 0) {
        return Promise.resolve({status: "success", item: regis});
      } else {
        const error = new Error("Please make sure your id is right");
        error.statusCode = 412;
        throw error;
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
  Regis.remoteMethod(
      "edit", {
        description: ["Return updated id"],
        accepts: [
          {arg: "id", type: "string", http: {source: 'path'}, required: true, description: "Id 5fa26188bd67d3df5407d018"},
          {arg: "data", type: "object", http: {source: 'body'}, required: true, description: "name, namePic, hpPic, address (object => street, region, city, district, postcode)"},
          {arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: {
          arg: "status", type: "object", root: true, description: "Return value"
        },
        http: {verb: "put", path: "/:id/edit"}
      }
  );

  Regis.add = async function (data, options, filter, skip, limit, sort) {
    // payload: {id: "string"}, {data}
      var app = require('../../server/server');
      const jadwalDokter = app.models.jadwal_dokter;
      const {connector, ObjectID}= Regis.getDataSource();
      const itemCollection = connector.collection('Regis');
      const $$polyId = ObjectID(filter['polyId']);
      const $$dokter = ObjectID(filter['dokterId']);
      const itemCursor = await itemCollection.aggregate([
        {
          $match:{
            //isActive:true
          }
        },
        {
          $lookup:{
            from: "poly",
            localField: "polyId",
            foreignField: "_id",
            as: "polys"
          }
        },
        {
          $unwind:{path: '$polys', preserveNullAndEmptyArrays: true},
        },
        {
          $lookup:{
            from: "jadwal_dokter",
            localField: "jadwal_dokterId",
            foreignField: "_id",
            as: "jadwal_dokters"
          },
        },
        {
          $unwind:{path: '$jadwal_dokters', preserveNullAndEmptyArrays: true},
        },
           {
          $lookup:{
            from: "dokter",
            localField: "jadwal_dokters.dokterId",
            foreignField: "_id",
            as: "dokters"
          },
        },
        {
          $unwind:{path: '$dokters', preserveNullAndEmptyArrays: true},
        },
        {
          $project:{
            _id : 1,
            nama : 1,
            nama_dokter:"$dokters.nama_dokter",
            hari:"$jadwal_dokters.hari",
            jam:"$jadwal_dokters.jam",
            nama_poly: "$polys.nama_poly",
            isActive: "$polys.isActive"
          }
        },
        {
          $match:{
            $or : [
              {hari : {$regex: data['hari'], $options: "i"}},
              {jam : {$regex: data['jam'], $options: "i"}},
            ]
          }
        }
      ])

      var cekJadwalDokter = await jadwalDokter.find({where:{hari:data['hari'], jam:data['jam'] }});
      console.log(cekJadwalDokter);

      if(cekJadwalDokter.length == "0"){
        return Promise.resolve({status: "dokter tersebut tidak peraktek di hari dan jam"})
      }
      var result = await itemCursor.toArray();
      //console.log(result.length);
      if(result.length != "0"){
        if(data['hari'] == result[0].hari && data['jam'] == result[0].jam){
          return Promise.resolve({status: "hari dan jam sudah di boking"})
        }
      }

    try {
      var regis = await Regis.create(data);
        return Promise.resolve({status: "success", item: regis});
      } catch (err) {
        return Promise.reject(err);
      }
     }
  Regis.remoteMethod(
      "add", {
        description: ["Return updated id"],
        accepts: [
          {arg: "data", type: "object", http: {source: 'body'}, required: true, description: "name, namePic, hpPic, address (object => street, region, city, district, postcode)"},
          {arg: "options", type: "object", http: "optionsFromRequest"}
        ],
        returns: {
          arg: "status", type: "object", root: true, description: "Return value"
        },
        http: {verb: "post", path: "/add"}
      }
  );
};



